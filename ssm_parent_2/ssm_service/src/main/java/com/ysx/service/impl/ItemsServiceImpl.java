package com.ysx.service.impl;

import com.ysx.dao.ItemsDao;
import com.ysx.pojo.Items.Items;
import com.ysx.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional//注解方式实现事务
@Component//为了能够让容器托管
public class ItemsServiceImpl implements ItemsService {

//    注入dao接口的代理对象
    @Autowired
    private ItemsDao itemDao;

    /**
     * 查询所有的记录封装集合返回
     * @return
     */
    public List<Items> getItems() {

        return  itemDao.getItems();
    }

    /**
     * 添加一条记录
     * @param item
     * @return
     */
    public int add(Items item) {

        return  itemDao.add(item);

    }
}

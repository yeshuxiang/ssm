import com.ysx.dao.ItemsDao;
import com.ysx.pojo.Items.Items;
import com.ysx.service.ItemsService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class mytest {
    @Test
    public void test_01() {
//        初始化容器
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-service.xml");
//        从容器中获取对象
        ItemsService bean = context.getBean(ItemsService.class);
//        查询所有的学科
        for (Items item : bean.getItems()) {
            System.out.println("item:" + item);
        }
//        新增一个学科
        Items items = new Items();
        items.setName("美食");
        int add = bean.add(items);
        if(add>0){
            System.out.println("添加成功！");
        }


    }
}

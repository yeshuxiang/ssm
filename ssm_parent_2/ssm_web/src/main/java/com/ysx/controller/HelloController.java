package com.ysx.controller;

import com.ysx.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
//    注入ItemsService代理类对象
    @Autowired
    private ItemsService itemsService;
    @RequestMapping("/hello")
    public String sayHello() {
//       测试是否能够连接spring和dao层
        System.out.println(itemsService.getItems());
//        测试是否能够连接mvc层
        return "success";
    }


}

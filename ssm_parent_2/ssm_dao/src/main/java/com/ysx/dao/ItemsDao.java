package com.ysx.dao;

import com.ysx.pojo.Items.Items;

import java.util.List;

public interface ItemsDao {
    /**
     * 查询所有的学科
     * @return
     */
    List<Items> getItems();


    /**
     * 新增一个学科
     */

    int add(Items item);


}

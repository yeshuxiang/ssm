import com.ysx.dao.ItemsDao;
import com.ysx.pojo.Items.Items;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

public class mytest {
    @Test
    public void test_01() {
//        初始化容器
ApplicationContext context = new ClassPathXmlApplicationContext("spring-dao.xml");
//        从容器中获取对象
        ItemsDao bean = context.getBean(ItemsDao.class);
//        查询所有的学科
        for (Items item : bean.getItems()) {
            System.out.println("item:" + item);
        }
//        新增一个学科
        Items items = new Items();
        items.setName("计算机");
        int add = bean.add(items);
        if(add>0){
            System.out.println("添加成功！");
        }


    }
    }




